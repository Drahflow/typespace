from common import Background
from enemies import Fighter, Boss
from objects import Text

def level1(objects, tick):
    if tick == 1:
        objects.append(Background())
        objects.append(Text("Level 1", 50))
        objects.append(Text("Zeigefinger auf f und j", 100))
    elif tick < 2000:
        if tick % 400 == 0:
            objects.append(Fighter("f", 100, 0))
            objects.append(Fighter("f", 150, 0))
            objects.append(Fighter("j", 650, 0))
            objects.append(Fighter("j", 700, 0))
    elif tick < 4000:
        if tick % 400 == 0:
            objects.append(Fighter("ff", 200, 0))
            objects.append(Fighter("ff", 250, 0))
            objects.append(Fighter("jj", 550, 0))
            objects.append(Fighter("jj", 600, 0))
    elif tick < 5200:
        if tick % 400 == 0:
            objects.append(Fighter("fj", 300, 0))
            objects.append(Fighter("jf", 500, 0))
    elif tick == 5200:
        objects.append(Boss("ffjjfjjf", 400, 0))
    elif tick == 7000:
        return True
