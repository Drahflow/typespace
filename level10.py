from enemies import Enemy, Shot
from common import *
from objects import Text

class LeftCannon(Enemy):
    def __init__(self):
        super().__init__("ei ei ei", 200, 50)
        self.y = 0
        self.exploding = 0

    def update(self, tick):
        if self.y < 150:
            self.y += 0.2

        if self.exploding > 0:
            if self.exploding == 30:
                explode(self.x, self.y, (255, 255, 0), 200, 1)
            if self.exploding == 1:
                explode(self.x, self.y, (255, 0, 0), 30, 4)
                objects.remove(self)

            self.exploding -= 1
        elif tick % 50 == 1:
            objects.append(Shot("a", self.x - 15, self.y + 20, -0.9, 0))
            objects.append(Shot("s", self.x - 5, self.y + 20, -0.4, 0.4))
            objects.append(Shot("d", self.x + 5, self.y + 20, 0.4, 0.4))
            objects.append(Shot("f", self.x + 15, self.y + 20, 0.9, 0))

    def draw(self):
        super().draw()
        pygame.draw.lines(screen, (255, 0, 0), True,
                ((self.x - 20, self.y),
                 (self.x, self.y - 20),
                 (self.x + 20, self.y),
                 (self.x, self.y + 20)
                ))

    def hit(self):
        self.exploding = 30

class RightCannon(Enemy):
    def __init__(self):
        super().__init__("ie ie ie", 600, 50)
        self.y = 0
        self.exploding = 0

    def update(self, tick):
        if self.y < 150:
            self.y += 0.2

        if self.exploding > 0:
            if self.exploding == 30:
                explode(self.x, self.y, (255, 255, 0), 200, 1)
            if self.exploding == 1:
                explode(self.x, self.y, (255, 0, 0), 30, 4)
                objects.remove(self)

            self.exploding -= 1
        elif tick % 50 == 1:
            objects.append(Shot("j", self.x - 15, self.y + 20, -0.9, 0))
            objects.append(Shot("k", self.x - 5, self.y + 20, -0.4, 0.4))
            objects.append(Shot("l", self.x + 5, self.y + 20, 0.4, 0.4))
            objects.append(Shot("ö", self.x + 15, self.y + 20, 0.9, 0))

    def draw(self):
        super().draw()
        pygame.draw.lines(screen, (255, 0, 0), True,
                ((self.x - 20, self.y),
                 (self.x, self.y - 20),
                 (self.x + 20, self.y),
                 (self.x, self.y + 20)
                ))

    def hit(self):
        self.exploding = 30


class Boss10(Enemy):
    def __init__(self):
        super().__init__("lustig", 400, 0)
        self.leftCannon = LeftCannon()
        objects.append(self.leftCannon)
        self.rightCannon = RightCannon()
        objects.append(self.rightCannon)

    def update(self, tick):
        if self.y < 150:
            self.y += 0.2

    def draw(self):
        super().draw()
        pygame.draw.lines(screen, (255, 0, 0), True,
                ((self.x - 250, self.y - 100),
                 (self.x + 250, self.y - 100),
                 (self.x + 250, self.y),
                 (self.x, self.y - 70),
                 (self.x - 250, self.y),
                ))

    def hit(self):
        score.inc(50)
        objects.remove(self)
        if self.leftCannon in objects:
            objects.remove(self.leftCannon)
        if self.rightCannon in objects:
            objects.remove(self.rightCannon)
        explode(self.x, self.y, (255, 255, 255), 100, 1)
        explode(self.x, self.y, (255, 255, 0), 200, 2)
        explode(self.x, self.y, (255, 0, 0), 200, 6)

def level10(objects, tick):
    if tick == 1:
        objects.append(Text("Level 10", 50))
        objects.append(Boss10())

    for obj in objects:
        if isinstance(obj, Enemy):
            return False

    return True
