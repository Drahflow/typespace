from enemies import Fighter, Shooter, Boss
from objects import Text
from random import random, choice

def level7(objects, tick):
    if tick == 1:
        objects.append(Text("Level 7", 50))
        objects.append(Text("Zeigefinger auch für g und h", 100))
    elif tick < 2000:
        if tick % 400 == 0:
            objects.append(Fighter("g", 100, 0))
            objects.append(Fighter("g", 150, 0))
            objects.append(Fighter("h", 650, 0))
            objects.append(Fighter("h", 700, 0))
    elif tick < 4000:
        if tick % 400 == 0:
            objects.append(Shooter("gd", 200, 0, "jf"))
            objects.append(Fighter("gg", 250, 0))
            objects.append(Fighter("hh", 550, 0))
            objects.append(Shooter("hk", 600, 0, "jf"))
    elif tick < 5200:
        if tick % 400 == 0:
            objects.append(Fighter("gh", 200, 0))
            objects.append(Fighter("gk", 250, 0))
            objects.append(Fighter("hg", 550, 0))
            objects.append(Fighter("hd", 600, 0))
    elif tick == 5200:
        objects.append(Boss("fhgj", 400, 0))
    elif tick == 6000:
        return True
