from enemies import Fighter, WordFighter, CrossFighter, Shooter, Boss
from objects import Text
from random import random, choice

# Needs 20
words = [
        "quer",
        "qualle",
        "quo",
        "qi",
        "quant",
        "po",
        "papa",
        "person",
        "stop",
        "pi",
        "popel",
        "top",
        "pott",
        "pass",
        "pelle",
        "puh",
        "pille",
        "app",
        "depp",
        "klippe",
        "kopf",
        "kuppel",
        "loipe",
        "quest",
]

def word():
    result = choice(words)
    words.remove(result)
    return result

def level12(objects, tick):
    if tick == 1:
        objects.append(Text("Level 12", 50))
        objects.append(Text("Kleine Finger auch für q und p", 100))
    elif tick < 2000:
        if tick % 400 == 0:
            objects.append(Fighter("q", 100, 0))
            objects.append(Fighter("q", 150, 0))
            objects.append(Fighter("p", 650, 0))
            objects.append(Fighter("p", 700, 0))
    elif tick < 4000:
        if tick % 400 == 0:
            objects.append(Shooter(word(), 200, 0, "asdfjklö"))
            objects.append(Shooter(word(), 600, 0, "asdfjklö"))
        if tick % 400 == 200:
            objects.append(WordFighter(word(), 250, 0))
            objects.append(WordFighter(word(), 550, 0))
    elif tick == 4000:
        objects.append(CrossFighter("quer", 800, 150))
    elif tick == 4200:
        objects.append(CrossFighter("fliegt", 800, 150))
    elif tick == 4400:
        objects.append(CrossFighter("wer", 800, 150))
    elif tick == 4600:
        objects.append(CrossFighter("siegt", 800, 150))
    elif tick == 5200:
        objects.append(Boss("papperlapapp", 400, 0))
    elif tick == 6000:
        return True
