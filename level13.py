from enemies import Fighter, WordFighter, CrossFighter, Shooter, Boss
from objects import Text
from random import random, choice

# Needs 20
words = [
        "tu",
        "toast",
        "hast",
        "otto",
        "otter",
        "taste",
        "test",
        "tee",
        "rest",
        "trasse",
        "platz",
        "putz",
        "zu",
        "zoo",
        "zeh",
        "eszet",
        "zettel",
        "zahl",
        "katze",
        "tatze",
        "pizza",
        "ozelot",
        "arzt",
        "putzt",
]

def word():
    result = choice(words)
    words.remove(result)
    return result

def level13(objects, tick):
    if tick == 1:
        objects.append(Text("Level 13", 50))
        objects.append(Text("Zeigefinger auch für t und z", 100))
    elif tick < 2000:
        if tick % 400 == 0:
            objects.append(Fighter("t", 100, 0))
            objects.append(Fighter("t", 150, 0))
            objects.append(Fighter("z", 650, 0))
            objects.append(Fighter("z", 700, 0))
    elif tick < 4000:
        if tick % 400 == 0:
            objects.append(Shooter(word(), 200, 0, "asdfjklö"))
            objects.append(Shooter(word(), 600, 0, "asdfjklö"))
        if tick % 400 == 200:
            objects.append(WordFighter(word(), 250, 0))
            objects.append(WordFighter(word(), 550, 0))
    elif tick == 4000:
        objects.append(CrossFighter("zwei", 800, 150))
    elif tick == 4200:
        objects.append(CrossFighter("worte", 800, 150))
    elif tick == 4400:
        objects.append(CrossFighter("ist", 800, 150))
    elif tick == 4600:
        objects.append(CrossFighter("gut", 800, 150))
    elif tick == 5200:
        objects.append(Boss("jetztzeit", 400, 0))
    elif tick == 6000:
        return True
