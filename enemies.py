from common import *
from random import random, choice

class Enemy:
    def __init__(self, text, x, y):
        self.x = x
        self.y = y
        self.text = text
        self.matched = 0

    def draw(self):
        txt = font.render(self.text[0:self.matched], True, (255, 0, 0))
        txtRest = font.render(self.text[self.matched:], True, (255, 255, 255))

        width = txt.get_width() + txtRest.get_width()
        screen.blit(txt, (self.x - width / 2, self.y))
        screen.blit(txtRest, (self.x - width / 2 + txt.get_width(), self.y))

    def update(self, tick):
        if self.text:
            self.y += 1 / len(self.text)
        else:
            self.y += 1

        if self.y > height:
            objects.remove(self)

    def typed(self, key):
        if not self.text:
            return

        if self.text[self.matched] != key:
            self.matched = 0
        if self.text[self.matched] == key:
            self.matched += 1

        if self.matched == len(self.text):
            self.text = ""
            shoot(self, self.x, self.y)
            return True

    def hit(self):
        objects.remove(self)
        explode(self.x, self.y, (255, 0, 0), 30, 2)

class Fighter(Enemy):
    img = pygame.transform.smoothscale(pygame.image.load("fighter1.png"), (30, 30))

    def __init__(self, text, x, y):
        super().__init__(text, x, y)

    def draw(self):
        if self.y < 0:
            return

        super().draw()
        screen.blit(self.img, (self.x - self.img.get_width() / 2, self.y - self.img.get_height()))

    def hit(self):
        score.inc(1)
        super().hit()

class BigFighter(Enemy):
    img = pygame.transform.smoothscale(pygame.image.load("fighter1.png"), (45, 45))

    def __init__(self, text, x, y):
        super().__init__(text, x, y)

    def draw(self):
        if self.y < 0:
            return

        super().draw()
        screen.blit(self.img, (self.x - self.img.get_width() / 2, self.y - self.img.get_height()))

    def hit(self):
        score.inc(3)
        super().hit()

class WordFighter(Enemy):
    img = pygame.transform.smoothscale(pygame.image.load("fighter2.png"), (30, 30))

    def __init__(self, text, x, y):
        super().__init__(text, x, y)

    def draw(self):
        if self.y < 0:
            return

        super().draw()
        screen.blit(self.img, (self.x - self.img.get_width() / 2, self.y - self.img.get_height()))

    def hit(self):
        score.inc(1)
        super().hit()

class CrossFighter(Enemy):
    img = pygame.transform.rotate(pygame.transform.smoothscale(pygame.image.load("fighter2.png"), (30, 30)), -90)

    def __init__(self, text, x, y):
        super().__init__(text, x, y)

    def update(self, tick):
        self.x -= 1

        if self.x < 0:
            objects.remove(self)

        if random() < 0.2:
            objects.append(Spark(self.x + 15, self.y - 23, random(), -0.5 + random(), (255, 255, 255 * random()), 1))
        if random() < 0.2:
            objects.append(Spark(self.x + 15, self.y - 10, random(), -0.5 + random(), (255, 255, 255 * random()), 1))

    def draw(self):
        if self.x < 0:
            return

        super().draw()
        screen.blit(self.img, (self.x - self.img.get_width() / 2, self.y - self.img.get_height()))

    def hit(self):
        score.inc(2)
        super().hit()

class Shot(Enemy):
    def __init__(self, text, x, y, dx, dy):
        super().__init__(text, x, y)
        self.dx = dx
        self.dy = dy
        self.v = sqrt(self.dx * self.dx + self.dy * self.dy)

    def update(self, tick):
        self.x += self.dx
        self.y += self.dy

        px = playerX - self.x
        py = playerY - self.y
        pv = sqrt(px * px + py * py)

        if pv < 20:
            score.dec(3)
            objects.remove(self)

        self.dx = 0.95 * self.dx + 0.05 * px / pv * self.v
        self.dy = 0.95 * self.dy + 0.05 * py / pv * self.v

        if self.y > height:
            objects.remove(self)

    def draw(self):
        super().draw()
        pygame.draw.lines(screen, (255, 255, 0), True,
                ((self.x - 5, self.y - 5),
                 (self.x + 5, self.y - 5),
                 (self.x, self.y - 10),
                 (self.x, self.y)
                ))

    def hit(self):
        objects.remove(self)
        explode(self.x, self.y, (255, 255, 0), 15, 1)

class Shooter(Enemy):
    img = pygame.transform.smoothscale(pygame.image.load("shooter1.png"), (30, 30))

    def __init__(self, text, x, y, shots):
        super().__init__(text, x, y)
        self.shots = shots

    def draw(self):
        if self.y < 0:
            return

        super().draw()
        screen.blit(self.img, (self.x - self.img.get_width() / 2, self.y - self.img.get_height()))

    def update(self, tick):
        super().update(tick)
        if tick % 200 == 49:
            objects.append(Shot(choice(self.shots), self.x, self.y, -2 + random() * 4, -2 + random() * 4))

    def hit(self):
        score.inc(3)
        super().hit()

class Boss(Enemy):
    img = pygame.transform.smoothscale(pygame.image.load("boss1.png"), (90, 60))

    def __init__(self, text, x, y):
        super().__init__(text, x, y)

    def draw(self):
        if self.y < 0:
            return

        super().draw()
        screen.blit(self.img, (self.x - self.img.get_width() / 2, self.y - self.img.get_height()))

    def hit(self):
        score.inc(10)
        super().hit()

class BigBoss(Enemy):
    img = pygame.transform.smoothscale(pygame.image.load("boss2.png"), (200, 125))

    def __init__(self, texts, x, y):
        super().__init__(texts[0], x, y)
        self.texts = texts[1:]

    def draw(self):
        if self.y < 0:
            return

        super().draw()
        screen.blit(self.img, (self.x - self.img.get_width() / 2, self.y - self.img.get_height()))

    def hit(self):
        if self.texts:
            self.text = self.texts.pop(0)
            self.matched = 0
        else:
            score.inc(30)
            objects.remove(self)

        explode(self.x, self.y, (255, 0, 0), 30, 2)

