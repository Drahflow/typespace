from enemies import Fighter, BigFighter, Boss
from objects import Text
from random import random, choice

def level6(objects, tick):
    if tick == 1:
        objects.append(Text("Level 6", 50))
        objects.append(Text("beide Daumen auf die Leertaste", 100))
    elif tick < 4000:
        if tick % 150 == 0:
            if random() < 0.5:
                objects.append(BigFighter(
                    choice([ "f f", "j j"]),
                    choice([100, 200, 600, 700]),
                    0))
            else:
                objects.append(Fighter(
                    choice(["a", "s", "d", "f"]) + choice(["j", "k", "l", "ö"]),
                    choice([100, 200, 600, 700]),
                    0))
    elif tick == 5200:
        objects.append(Boss("f f j j", 400, 0))
    elif tick == 6000:
        return True
