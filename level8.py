from enemies import Fighter, Shooter, Boss
from objects import Text
from random import random, choice

def level8(objects, tick):
    if tick == 1:
        objects.append(Text("Level 8", 50))
        objects.append(Text("Zeigefinger auch für r und u", 100))
    elif tick < 2000:
        if tick % 400 == 0:
            objects.append(Fighter("r", 100, 0))
            objects.append(Fighter("r", 150, 0))
            objects.append(Fighter("u", 650, 0))
            objects.append(Fighter("u", 700, 0))
    elif tick < 4000:
        if tick % 400 == 0:
            objects.append(Shooter("rd", 200, 0, "jfaö"))
            objects.append(Fighter("rr", 250, 0))
            objects.append(Fighter("uu", 550, 0))
            objects.append(Shooter("uk", 600, 0, "jfaö"))
    elif tick < 5200:
        if tick % 400 == 0:
            objects.append(Fighter("rs", 200, 0))
            objects.append(Fighter("rf", 250, 0))
            objects.append(Fighter("uj", 550, 0))
            objects.append(Fighter("ul", 600, 0))
    elif tick == 5200:
        objects.append(Boss(choice(["fahr", "fuhr"]), 400, 0))
    elif tick == 6000:
        return True
