from enemies import Fighter, Boss
from common import score
import sys

def won(objects, tick):
    if tick == 1:
        objects.append(Fighter("Gewonnen!", 400, 0))
    if tick == 1000:
        print("Gesamtpunktzahl: %.3f" % (score.get(),))
        sys.exit(0)

