#!/usr/bin/env python3

import pygame_sdl2
pygame_sdl2.import_as_pygame()
import sys, pygame
pygame.init()
pygame.font.init()

from enemies import Enemy

from common import *

class Player:
    def draw(self):
        pygame.draw.lines(screen, (255, 255, 255), True,
                ((playerX, playerY),
                 (playerX + 10, playerY + 30),
                 (playerX - 10, playerY + 30),
                ))

    def update(self, tick):
        pass

    def typed(self, key):
        pass

objects.append(Player())

from level1 import level1
from level2 import level2
from level3 import level3
from level4 import level4
from level5 import level5
from level6 import level6
from level7 import level7
from level8 import level8
from level9 import level9
from level10 import level10
from level11 import level11
from level12 import level12
from level13 import level13
from won import won

levels = [level1, level2, level3, level4, level5, level6, level7, level8, level9, level10, level11, level12, level13, won]
# levels = [level12, won]
level = levels.pop(0)
clock = pygame.time.Clock()
completed = False
tick = 0
while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.unicode:
                for obj in objects:
                    if obj.typed(event.unicode):
                        for obj in objects:
                            obj.typed(chr(0))
                        break

    for obj in objects:
        obj.update(tick)

    screen.fill((0, 0, 0))
    for obj in objects:
        obj.draw()

    pygame.display.flip()
    clock.tick(30)
    while True:
        tick += 1

        completed = level(objects, tick)
        if completed:
            level = levels.pop(0)
            tick = 0
            completed = False
            break

        enemiesActive = False
        for obj in objects:
            if isinstance(obj, Enemy):
                enemiesActive = True
                break

        if enemiesActive:
            break

        score.inc(0.01)
