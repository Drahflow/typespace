from enemies import Fighter, Boss, Shooter
from objects import Text

def level3(objects, tick):
    if tick == 1:
        objects.append(Text("Level 3", 50))
        objects.append(Text("Ringfinger auf s und l", 100))
    elif tick < 2000:
        if tick % 400 == 0:
            objects.append(Fighter("s", 100, 0))
            objects.append(Fighter("s", 150, 0))
            objects.append(Fighter("l", 650, 0))
            objects.append(Fighter("l", 700, 0))
    elif tick < 4000:
        if tick % 400 == 0:
            objects.append(Shooter("ss", 200, 0, "jf"))
            objects.append(Fighter("ss", 250, 0))
            objects.append(Fighter("ll", 550, 0))
            objects.append(Shooter("ll", 600, 0, "jf"))
    elif tick < 5200:
        if tick % 400 == 0:
            objects.append(Fighter("sl", 300, 0))
            objects.append(Fighter("ls", 500, 0))
    elif tick == 5200:
        objects.append(Boss("ssllslls", 400, 0))
    elif tick == 6000:
        return True
