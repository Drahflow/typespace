from enemies import Fighter, WordFighter, Shooter, Boss
from objects import Text
from random import random, choice

# Needs 20
words = [
        "wo",
        "wer",
        "was",
        "wieso",
        "wert",
        "wort",
        "wasser",
        "ort",
        "rot",
        "tor",
        "rotor",
        "foto",
        "hort",
        "wohl",
        "hol",
        "löwe",
        "weder",
        "los",
        "wie",
        "weil",
        "sowohl",
        "doof",
        "sofort",
        "warte",
]

def word():
    result = choice(words)
    words.remove(result)
    return result

def level11(objects, tick):
    if tick == 1:
        objects.append(Text("Level 11", 50))
        objects.append(Text("Ringfinger auch für w und o", 100))
    elif tick < 2000:
        if tick % 400 == 0:
            objects.append(Fighter("w", 100, 0))
            objects.append(Fighter("w", 150, 0))
            objects.append(Fighter("o", 650, 0))
            objects.append(Fighter("o", 700, 0))
    elif tick < 4000:
        if tick % 400 == 0:
            objects.append(Shooter(word(), 200, 0, "asdfjklö"))
            objects.append(Shooter(word(), 600, 0, "asdfjklö"))
        if tick % 400 == 200:
            objects.append(WordFighter(word(), 250, 0))
            objects.append(WordFighter(word(), 550, 0))
    elif tick == 4000:
        if tick % 400 == 0:
            objects.append(WordFighter("was", 200, 0))
            objects.append(WordFighter("ist", 250, 0))
            objects.append(WordFighter("hier", 550, 0))
            objects.append(WordFighter("los", 600, 0))
    elif tick == 5200:
        objects.append(Boss("wieso das alles", 400, 0))
    elif tick == 6000:
        return True
