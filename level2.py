from enemies import Fighter, Boss, Shooter
from objects import Text

def level2(objects, tick):
    if tick == 1:
        objects.append(Text("Level 2", 50))
        objects.append(Text("Mittelfinger auf d und k", 100))
    elif tick < 2000:
        if tick % 400 == 0:
            objects.append(Fighter("d", 100, 0))
            objects.append(Fighter("d", 150, 0))
            objects.append(Fighter("k", 650, 0))
            objects.append(Fighter("k", 700, 0))
    elif tick < 4000:
        if tick % 400 == 0:
            objects.append(Shooter("dd", 200, 0, "jf"))
            objects.append(Fighter("dd", 250, 0))
            objects.append(Fighter("kk", 550, 0))
            objects.append(Shooter("kk", 600, 0, "jf"))
    elif tick < 5200:
        if tick % 400 == 0:
            objects.append(Fighter("dk", 300, 0))
            objects.append(Fighter("kd", 500, 0))
    elif tick == 5200:
        objects.append(Boss("ddkkdkkd", 400, 0))
    elif tick == 6000:
        return True
