from enemies import Fighter, Shooter, Boss
from objects import Text
from random import random, choice

def level9(objects, tick):
    if tick == 1:
        objects.append(Text("Level 9", 50))
        objects.append(Text("Mittelfinger auch für e und i", 100))
    elif tick < 2000:
        if tick % 400 == 0:
            objects.append(Fighter("e", 100, 0))
            objects.append(Fighter("e", 150, 0))
            objects.append(Fighter("i", 650, 0))
            objects.append(Fighter("i", 700, 0))
    elif tick < 4000:
        if tick % 400 == 0:
            objects.append(Shooter("erde", 200, 0, "asdfjklö"))
            objects.append(Fighter("er", 250, 0))
            objects.append(Fighter("ui", 550, 0))
            objects.append(Shooter("lilie", 600, 0, "asdfjklö"))
    elif tick < 5200:
        if tick % 400 == 0:
            objects.append(Fighter("die", 200, 0))
            objects.append(Fighter("erde", 250, 0))
            objects.append(Fighter("sah", 550, 0))
            objects.append(Fighter("reis", 600, 0))
    elif tick == 5200:
        objects.append(Boss("sei leiser", 400, 0))
    elif tick == 6000:
        return True
