from common import *

class Text:
    def __init__(self, text, y):
        self.text = text
        self.y = y
        self.t = 0

    def draw(self):
        txt = font.render(self.text, True, (0, 0, 255))

        width = txt.get_width()
        screen.blit(txt, (400 - width / 2, self.y))

    def update(self, tick):
        self.t += 1

        if self.t > 100:
            objects.remove(self)

    def typed(self, key):
        pass
