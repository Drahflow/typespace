from enemies import BigBoss
from objects import Text

def level5(objects, tick):
    if tick == 1:
        objects.append(Text("Level 5", 50))
    elif tick == 100:
        objects.append(BigBoss(["asdf", "jklö", "lass", "das", "fass"], 400, 0))
    elif tick == 10000:
        return True
