from enemies import Fighter, Boss, Shooter
from objects import Text

def level4(objects, tick):
    if tick == 1:
        objects.append(Text("Level 4", 50))
        objects.append(Text("kleine Finger auf a und ö", 100))
    elif tick < 2000:
        if tick % 400 == 0:
            objects.append(Fighter("a", 100, 0))
            objects.append(Fighter("a", 150, 0))
            objects.append(Fighter("ö", 650, 0))
            objects.append(Fighter("ö", 700, 0))
    elif tick < 4000:
        if tick % 400 == 0:
            objects.append(Shooter("aa", 200, 0, "jf"))
            objects.append(Fighter("aa", 250, 0))
            objects.append(Fighter("öö", 550, 0))
            objects.append(Shooter("öö", 600, 0, "jf"))
    elif tick < 5200:
        if tick % 400 == 0:
            objects.append(Fighter("aö", 300, 0))
            objects.append(Fighter("öa", 500, 0))
    elif tick == 5200:
        objects.append(Boss("aaööaööa", 400, 0))
    elif tick == 6000:
        return True
