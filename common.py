import pygame

from math import sin, cos, sqrt
from random import random

size = width, height = 800, 600
screen = pygame.display.set_mode(size)
font = pygame.font.Font(pygame.font.match_font("courier", bold=True), 20)
playerX, playerY = (400, 550)
objects = []

class PlayerShot:
    def __init__(self, target, x, y):
        self.x = x
        self.y = y
        self.target = target
        self.step = 0
        self.color = (255, 255, 0)
        dx = playerX - x
        dy = playerY - y
        self.stepMax = sqrt(dx * dx + dy * dy) / 50

    def draw(self):
        t = self.step / self.stepMax
        x = t * self.x + (1 - t) * playerX
        y = t * self.y + (1 - t) * playerY
        pygame.draw.rect(screen, self.color, (x - 1, y - 1, 3, 3))

    def update(self, tick):
        self.step += 1

        if self.step >= self.stepMax:
            objects.remove(self)
            self.target.hit()

    def typed(self, key):
        pass

class Spark:
    def __init__(self, x, y, dx, dy, color, size):
        self.x = x
        self.y = y
        self.dx = dx
        self.dy = dy
        self.color = color
        self.duration = 20
        self.size = size / 2

    def draw(self):
        pygame.draw.rect(screen, self.color, (self.x - self.size, self.y - self.size, self.size * 2, self.size * 2))

    def update(self, tick):
        self.x += self.dx
        self.y += self.dy
        self.duration -= 1
        if self.duration < 0:
            objects.remove(self)

    def typed(self, key):
        pass

class Score:
    def __init__(self, points):
        self.points = points

    def draw(self):
        txt = font.render(str(int(self.points)), True, (128, 128, 128))

        width = txt.get_width()
        screen.blit(txt, (790 - width, 10))

    def update(self, tick):
        pass

    def typed(self, key):
        pass

    def inc(self, amount):
        self.points += amount

    def dec(self, amount):
        self.points -= amount

    def get(self):
        return self.points

class Background:
    def update(self, tick):
        pass

    def draw(self):
        pass

    def typed(self, key):
        pass

def explode(x, y, color, count, size):
    for i in range(0, count):
        a = 6.282 * random()
        v = 5 * random() + 2
        dx = v * cos(a)
        dy = v * sin(a)
        objects.append(Spark(x, y, dx, dy, color, size))

def shoot(what, x, y):
    objects.append(PlayerShot(what, x, y))

score = Score(0)
objects.append(score)
